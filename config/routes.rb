Rails.application.routes.draw do
  resources :employees do
    post :import, on: :collection
  end
  root "home#index"
  get '/payslips', to: 'payslips#index', as: :payslips
  post '/payslips/multiple', to: 'payslips#show_multiple', as: :payslips_multiple
  get '/payslips/multiple', to: 'payslips#show_multiple', as: :payslips_export_csv
end
