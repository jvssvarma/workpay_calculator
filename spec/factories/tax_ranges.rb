FactoryGirl.define do
  factory :tax_range do
    income_min 0
    income_max 19200
    rate "9.99"
    tax_from_lower_range 0
  end

  factory :tax_range2, class: TaxRange do
    income_min 37001
    income_max 80000
    rate "0.325"
    tax_from_lower_range 3572
  end
end
