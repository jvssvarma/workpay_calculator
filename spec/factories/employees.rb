FactoryGirl.define do
  factory :employee do
    first_name "John"
    last_name "Doe"
    annual_salary '1231'
    super_rate "9%"
  end
end
