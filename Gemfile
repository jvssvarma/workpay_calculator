source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.6'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0', group: :doc

gem 'bootstrap-sass'
gem 'jquery-turbolinks', '~> 2.1'
gem 'haml'
gem 'haml-rails', '~> 0.9.0'
gem 'crummy', '~> 1.8'
gem 'forgery', '~> 0.6.0'
gem 'business_time', '~> 0.7.6'
gem 'puma'

group :development, :test do
  gem 'byebug'
  gem 'sqlite3'
  gem 'pry-remote', '~> 0.1.8'
  gem 'pry-rails', '~> 0.3.4'
  gem 'pry-stack_explorer', '~> 0.4.9.2'
  gem 'pry-nav', '~> 0.2.4'
  gem 'rspec-rails', '~> 3.5', '>= 3.5.2'
  gem 'rspec-its', '~> 1.2'
  gem 'libnotify', '~> 0.9.1'
  gem 'rb-fsevent', '~> 0.9.8'
  gem 'rb-inotify', '~> 0.9.7'
  gem 'fuubar', '~> 2.2'
  gem 'factory_girl_rails', '~> 4.7'
  gem 'rubocop', '~> 0.45.0', require: false
end

group :development do
  gem 'annotate', '~> 2.7', '>= 2.7.1'
  gem 'better_errors', '~> 2.1', '>= 2.1.1'
  gem 'quiet_assets', '~> 1.1'
  gem 'hirb', '~> 0.7.3'
  gem 'binding_of_caller', '~> 0.7.2'
  gem 'awesome_print', '~> 1.7'
  gem 'guard', '~> 2.14'
  gem 'meta_request', '~> 0.4.0'
  gem 'guard-rspec', '~> 4.7', '>= 4.7.3'
  gem 'guard-cucumber', '~> 2.1.2'
  gem 'web-console', '~> 2.0'
  gem 'spring'
end

group :test do
  gem 'capybara', '~> 2.10', '>= 2.10.1'
  gem 'poltergeist', '~> 1.11'
  gem 'launchy', '~> 2.4', '>= 2.4.3'
  gem 'shoulda-matchers', '~> 3.1', '>= 3.1.1'
  gem 'database_cleaner', '~> 1.5', '>= 1.5.3'
  gem 'faker', '~> 1.6', '>= 1.6.6'
  gem 'selenium-webdriver', '~> 3.0'
  gem 'simplecov', require: false
end

group :production do
  gem 'rails_12factor'
  gem 'pg'
end
